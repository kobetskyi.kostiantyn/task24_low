﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Intefaces
{
    interface IUnitOfWork
    {
        IArticleRepository ArticleRepository { get; }
        IAuthorRepository AuthorRepository { get; }
        ICommentRepository CommentRepository { get; }
        IQuestionnaireRepository QuestionnaireRepository { get; }
        Task<int> SaveAsync();
    }
}
