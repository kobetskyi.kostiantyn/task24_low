﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities.Abstract;

namespace Data.Entities
{
    public class Comment:BaseEntity
    {
        public string Text { get; set; }
        public DateTime DateTime { get; set; }
        public string Name { get; set; }

    }
}
