﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library_Epam.TestData;

namespace Library_Epam.Controllers
{
    public class CommentsController : Controller
    {
        // GET: Comments
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Comments = TestData.Data.Comments;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string Name, string Text)
        {
            TestData.Data.Comments.Add(new Comment()
            {
                Author = Name,
                Text = Text,
                Date = DateTime.Now,
            });
           
            return RedirectToAction("Index");
        }
    }
}