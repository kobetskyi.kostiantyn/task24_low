﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Repositories
{
    public class CommentRepository:Repository<Comment>
    {
        public CommentRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }
    }
}
