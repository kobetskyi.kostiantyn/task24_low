﻿using System.Linq;
using System.Threading.Tasks;
using Library_Epam.DAL.Entities;
using Library_Epam.DAL.Intefaces;

namespace Library_Epam.DAL.Repositories
{
    public class QuestionnaireRepository : Repository<Questionnaire>, IQuestionnaireRepository
    {
        public QuestionnaireRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Questionnaire> FindAllWithDetails()
        {
            return dbContext.Questionnaires.Include("OddNumbers");
        }

        public async Task<Questionnaire> FindByIdWithDetails(int id)
        {
            var questionnaire = await dbContext.Questionnaires.FindAsync(id);
            await dbContext.Entry(questionnaire).Reference("OddNumbers").LoadAsync();
            return questionnaire;
        }

    }
}
