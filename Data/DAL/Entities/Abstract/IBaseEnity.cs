﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities.Abstract
{
    public interface IBaseEntity
    {
        int Id { get; set; }
    }
}
