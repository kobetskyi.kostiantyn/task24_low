﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Library_Epam.DAL;
using Library_Epam.DAL.Entities;

namespace Library_Epam.Controllers
{
    public class HomeController : Controller
    {
        private UnitOfWork _unitOfWork;

        public HomeController()
        {

            _unitOfWork = new UnitOfWork(ConfigurationManager.ConnectionStrings["DBConnection"].ToString());
        }

        public async Task<ActionResult> Index()
        {

            //await _unitOfWork.AuthorRepository.DeleteByIdAsync(4);
            //await _unitOfWork.SaveAsync();
            

            return View(TestData.Data.Articles);
        }
    }
}