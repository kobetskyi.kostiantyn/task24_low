﻿using System.Threading.Tasks;

namespace Library_Epam.DAL.Intefaces
{
    interface IUnitOfWork
    {
        IArticleRepository ArticleRepository { get; }
        IAuthorRepository AuthorRepository { get; }
        ICommentRepository CommentRepository { get; }
        IQuestionnaireRepository QuestionnaireRepository { get; }
        Task<int> SaveAsync();
    }
}
