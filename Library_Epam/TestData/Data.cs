﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library_Epam.TestData
{
    public static class Data
    {
        public static List<Comment> Comments { get; } = new List<Comment>()
        {
            new Comment()
            {
                Author = "Kostya",
                Date = new DateTime(2021, 06, 12),
                Text = "Minus nemo amet perferendis accusantium expedita? " +
                       "Impedit, ipsum sint harum facere dicta porro aliquid" +
                       " nesciunt aspernatur perspiciatis autem maxime eligendi"
            },
            new Comment()
            {
                Author = "Vasya",
                Date = new DateTime(2021, 06, 15),
                Text = "Impedit, ipsum sint harum facere dicta porro " +
                       "aliquid nesciunt aspernatur perspiciatis autem maxime " +
                       "eligendi placeat, quas amet ratione perferendis accusantium, ",
                      
            },

            new Comment()
            {
                Author = "Petya",
                Date = new DateTime(2021, 06, 18),
                Text = "Aliquid nesciunt aspernatur perspiciatis autem maxime " +
                       "eligendi placeat, quas amet ratione perferendis accusantium",

            },



        };
        public static List<Article> Articles { get; } = new List<Article>()
        {
            new Article()
            {
                Title = "Returns an anchor element",
                Author = "Kostya",
                Text = "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laborum velit" +
                       "earum eos maiores amet beatae reprehenderit explicabo eligendi aperiam, magni placeat" +
                       "pariatur harum odio sequi nisi modi cupiditate qui in.",
                Date = new DateTime(2020, 11, 15),
            },

            new Article()
            {
                Title = "ActionLink creates a hyperlink",
                Author = "Vasya",
                Text = "Minus nemo amet perferendis accusantium expedita? Impedit," +
                       " ipsum sint harum facere dicta porro aliquid nesciunt aspernatur" +
                       " perspiciatis autem maxime eligendi placeat, quas amet ratione" +
                       " perferendis accusantium, labore nihil dolores quasi. Iusto.",
                Date = new DateTime(2018, 06, 05),
            },
            
            new Article()
            {
                Title = "Controller Name",
                Author = "Vasya",
                Text = "Numquam nesciunt, inventore placeat in, autem minima ut aliquam " +
                       "odio deleniti officia, dolor dolorum quas sequi iste? Obcaecati " +
                       "veritatis ipsa soluta error.",
                Date = new DateTime(2018, 06, 05),
            },


        };
    }

    public class Article
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }

    public class Comment
    {
        public string Author { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
    }
}