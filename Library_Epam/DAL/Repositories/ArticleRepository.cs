﻿using System.Linq;
using System.Threading.Tasks;
using Library_Epam.DAL.Entities;
using Library_Epam.DAL.Intefaces;

namespace Library_Epam.DAL.Repositories
{
    public class ArticleRepository:Repository<Article>, IArticleRepository
    {
        public ArticleRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }
        public IQueryable<Article> FindAllWithDetails()
        {
            return dbContext.Articles.Include("Author");
        }

        public async Task<Article> GetByIdWithDetailsAsync(int id)
        {
            var article = await dbContext.Articles.FindAsync(id);
            await dbContext.Entry(article).Reference("Author").LoadAsync();
            return article;
        }
    }
}
