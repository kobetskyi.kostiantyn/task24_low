﻿using Data.Entities.Abstract;

namespace Data.Entities
{
    public class OddNumbers:BaseEntity
    {
        public byte One { get; set; }
        public byte Two { get; set; }
        public byte Three { get; set; }

    }
}