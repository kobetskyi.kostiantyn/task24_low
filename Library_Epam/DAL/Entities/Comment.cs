﻿using System;
using Library_Epam.DAL.Entities.Abstract;

namespace Library_Epam.DAL.Entities
{
    public class Comment:BaseEntity
    {
        public string Text { get; set; }
        public DateTime DateTime { get; set; }
        public string Name { get; set; }

    }
}
