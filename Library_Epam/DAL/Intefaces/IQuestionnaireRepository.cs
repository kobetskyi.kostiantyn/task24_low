﻿using System.Linq;
using System.Threading.Tasks;
using Library_Epam.DAL.Entities;

namespace Library_Epam.DAL.Intefaces
{
    public interface IQuestionnaireRepository :IRepository<Questionnaire>
    {
        IQueryable<Questionnaire> FindAllWithDetails();
        Task<Questionnaire> FindByIdWithDetails(int id);
    }
}
