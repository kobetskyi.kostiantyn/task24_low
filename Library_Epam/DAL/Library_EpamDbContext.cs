﻿using System;
using System.Data.Entity;
using Library_Epam.DAL.Entities;

namespace Library_Epam.DAL
{
    public class Library_EpamDbContext : DbContext
    {

        static Library_EpamDbContext()
        {
            Database.SetInitializer<Library_EpamDbContext>(
                new LibraryEpamDbInitializer());
        }
        public Library_EpamDbContext(string connectionString)
            : base(connectionString) { }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<OddNumbers> OddNumbers { get; set; }

    }

    public class LibraryEpamDbInitializer : DropCreateDatabaseAlways<Library_EpamDbContext>
    {
        protected override void Seed(Library_EpamDbContext context)
        {
            context.Authors.Add(new Author() { Id = 1, Name = "Kostya" });
            context.Authors.Add(new Author() { Id = 2, Name = "Vanya" });
            context.Authors.Add(new Author() { Id = 3, Name = "Sasha" });

            context.Articles.AddRange(new[]
            {
                new Article()
                {
                    Name = "Returns an anchor element",
                    AuthorId = 1,
                    Text = "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laborum velit" +
                           "earum eos maiores amet beatae reprehenderit explicabo eligendi aperiam, magni placeat" +
                           "pariatur harum odio sequi nisi modi cupiditate qui in.",
                    DateTime = new DateTime(2020, 11, 15),
                },
                new Article()
                {
                    Name = "ActionLink creates a hyperlink",
                    AuthorId = 2,
                    Text = "Minus nemo amet perferendis accusantium expedita? Impedit," +
                           " ipsum sint harum facere dicta porro aliquid nesciunt aspernatur" +
                           " perspiciatis autem maxime eligendi placeat, quas amet ratione" +
                           " perferendis accusantium, labore nihil dolores quasi. Iusto.",
                    DateTime = new DateTime(2018, 06, 05),
                },
                new Article()
                {
                    Name = "Controller Name",
                    AuthorId = 3,
                    Text = "Numquam nesciunt, inventore placeat in, autem minima ut aliquam " +
                           "odio deleniti officia, dolor dolorum quas sequi iste? Obcaecati " +
                           "veritatis ipsa soluta error.",
                    DateTime = new DateTime(2018, 06, 05),
                },
            });

            context.Comments.AddRange(new[]
            {
                new Comment()
                {
                    Name = "Kostya",
                    DateTime = new DateTime(2021, 06, 12),
                    Text = "Minus nemo amet perferendis accusantium expedita? " +
                           "Impedit, ipsum sint harum facere dicta porro aliquid" +
                           " nesciunt aspernatur perspiciatis autem maxime eligendi"
                },
                new Comment()
                {
                    Name = "Vasya",
                    DateTime = new DateTime(2021, 06, 15),
                    Text = "Impedit, ipsum sint harum facere dicta porro " +
                           "aliquid nesciunt aspernatur perspiciatis autem maxime " +
                           "eligendi placeat, quas amet ratione perferendis accusantium, ",

                },
                new Comment()
                {
                    Name = "Petya",
                    DateTime = new DateTime(2021, 06, 18),
                    Text = "Aliquid nesciunt aspernatur perspiciatis autem maxime " +
                           "eligendi placeat, quas amet ratione perferendis accusantium",

                },
            });
            context.SaveChanges();
        }
    }
}
