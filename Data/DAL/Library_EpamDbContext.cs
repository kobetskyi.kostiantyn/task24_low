﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using Data.Entities;


namespace Data
{
    public class Library_EpamDbContext : DbContext
    {

        static Library_EpamDbContext()
        {
            Database.SetInitializer<Library_EpamDbContext>(
                new Library_EpamDbInitializer());
        }
        public Library_EpamDbContext(string connectionString)
            : base(connectionString) { }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<OddNumbers> OddNumbers { get; set; }

    }

    public class Library_EpamDbInitializer : DropCreateDatabaseIfModelChanges<Library_EpamDbContext>
    {
        protected override void Seed(Library_EpamDbContext context)
        {
            context.Authors.Add(new Author() { Id = 1, Name = "Kostya" });
            context.Authors.Add(new Author() { Id = 2, Name = "Vanya" });
            context.Authors.Add(new Author() { Id = 3, Name = "Sasha" });
            
        }
    }
}
