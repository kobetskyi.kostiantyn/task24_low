﻿using System.Linq;
using System.Threading.Tasks;
using Library_Epam.DAL.Entities;

namespace Library_Epam.DAL.Intefaces
{
    public interface IArticleRepository : IRepository<Article>
    {
        IQueryable<Article> FindAllWithDetails();
        Task<Article> GetByIdWithDetailsAsync(int id);
    }
}
