﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Repositories
{
    public class OddNumbersRepository:Repository<OddNumbers>
    {
        public OddNumbersRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }
    }
}
