﻿namespace Library_Epam.DAL.Entities.Abstract
{
    public interface IBaseEntity
    {
        int Id { get; set; }
    }
}
