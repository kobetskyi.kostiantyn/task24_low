﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Intefaces
{
    public interface IArticleRepository : IRepository<Article>
    {
        IQueryable<Article> FindAllWithDetails();
        Task<Article> GetByIdWithDetailsAsync(int id);
    }
}
