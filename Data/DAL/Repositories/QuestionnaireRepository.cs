﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Repositories
{
    public class QuestionnaireRepository : Repository<Questionnaire>
    {
        public QuestionnaireRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Questionnaire> FindAllWithDetails()
        {
            return dbContext.Questionnaires.Include("OddNumbers");
        }

        public async Task<Questionnaire> FindByIdWithDetails(int id)
        {
            var questionnaire = await dbContext.Questionnaires.FindAsync(id);
            await dbContext.Entry(questionnaire).Reference("OddNumbers").LoadAsync();
            return questionnaire;
        }

    }
}
