﻿using Library_Epam.DAL.Entities;

namespace Library_Epam.DAL.Intefaces
{
    public interface IOddNumbersRepository:IRepository<OddNumbers>
    {
    }
}
