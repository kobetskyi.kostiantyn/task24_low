﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;
using Data.Intefaces;

namespace Data.Repositories
{
    public class ArticleRepository:Repository<Article>, IArticleRepository
    {
        public ArticleRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }
        public IQueryable<Article> FindAllWithDetails()
        {
            return dbContext.Articles.Include("Author");
        }

        public async Task<Article> GetByIdWithDetailsAsync(int id)
        {
            var article = await dbContext.Articles.FindAsync(id);
            await dbContext.Entry(article).Reference("Author").LoadAsync();
            return article;
        }
    }
}
