﻿using System.Linq;
using System.Threading.Tasks;

namespace Library_Epam.DAL.Intefaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        Task<T> GetByIdAsync(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        Task DeleteByIdAsync(int id);
    }
}
