﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Library_Epam.DAL.Entities.Abstract;

namespace Library_Epam.DAL.Entities
{
    public class Article:BaseEntity
    {
        public string Name { get; set; }
        public string Text { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime DateTime { get; set; }
        public int AuthorId { get; set; }
        public Author Author{ get; set; }
    }
}
