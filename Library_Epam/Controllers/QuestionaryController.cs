﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Library_Epam.Controllers
{
    public class QuestionaryController : Controller
    {
        // GET: Questionary
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Colors = new string[]
            {
                "Green", "Blue", "Red"
            };
            ViewBag.Characters = new[]
            {
                "Storm", "Wolwerine", "Cyclopus", "Tick",
            };
            return View();
        }


        [HttpPost]
        public ActionResult Index(string Sex, string SuperFluous, string Colors, bool One, bool Two, bool Three)
        {
            return RedirectToAction("Result", new { sex = Sex, character = SuperFluous, color = Colors, n1 = One, n2 = Two, n3 = Three });
        }

        public ActionResult Result(string sex, string character, string color, bool n1, bool n2, bool n3)
        {
            List<string> oddNumbers = new List<string>();

            if (n1)
                oddNumbers.Add("One");
            if (n2)
                oddNumbers.Add("Two");
            if (n3)
                oddNumbers.Add("Three");


            ViewBag.OddNumbers = oddNumbers.ToArray();
            ViewBag.Sex = sex;
            ViewBag.Character = character;
            ViewBag.Color = color;
            return View();
        }
    }
}