﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities.Abstract;

namespace Data.Entities
{
    public class Questionnaire:BaseEntity
    {
        public string FavoriteColor { get; set; }
        public string Sex { get; set; }
        public string Character { get; set; }
        public int OddNumbersIs { get; set; }
        public OddNumbers OddNumbers { get; set; }
    }
}
