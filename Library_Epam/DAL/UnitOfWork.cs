﻿using System;
using System.Threading.Tasks;
using Library_Epam.DAL.Intefaces;
using Library_Epam.DAL.Repositories;

namespace Library_Epam.DAL
{
    class UnitOfWork : IUnitOfWork
    {

        private Library_EpamDbContext dbContext;
        private IArticleRepository articleRepository;
        private IAuthorRepository authorRepository;
        private ICommentRepository commentRepository;
        private IQuestionnaireRepository questionnaireRepository;


        public UnitOfWork(string connectionString) => dbContext = new Library_EpamDbContext(connectionString);


        public IArticleRepository ArticleRepository
        {
            get { return articleRepository ?? (articleRepository = new ArticleRepository(dbContext)); }
        }

        public IAuthorRepository AuthorRepository
        {
            get { return authorRepository ?? (authorRepository = new AuthorRepository(dbContext)); }
        }

        public ICommentRepository CommentRepository
        {
            get { return commentRepository ?? (commentRepository = new CommentRepository(dbContext)); }
        }

        public IQuestionnaireRepository QuestionnaireRepository
        {
            get { return questionnaireRepository ?? (questionnaireRepository = new QuestionnaireRepository(dbContext)); }
        }

        public async Task<int> SaveAsync() => await dbContext.SaveChangesAsync();
    }
}
