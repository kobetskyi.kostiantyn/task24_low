﻿using Library_Epam.DAL.Entities;
using Library_Epam.DAL.Intefaces;

namespace Library_Epam.DAL.Repositories
{
    public class AuthorRepository:Repository<Author>, IAuthorRepository
    {
        public AuthorRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }

    }
}
