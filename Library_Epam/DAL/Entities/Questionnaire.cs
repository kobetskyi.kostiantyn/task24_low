﻿using Library_Epam.DAL.Entities.Abstract;

namespace Library_Epam.DAL.Entities
{
    public class Questionnaire:BaseEntity
    {
        public string FavoriteColor { get; set; }
        public string Sex { get; set; }
        public string Character { get; set; }
        public int OddNumbersIs { get; set; }
        public OddNumbers OddNumbers { get; set; }
    }
}
