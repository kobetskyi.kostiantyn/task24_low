﻿using Library_Epam.DAL.Entities;
using Library_Epam.DAL.Intefaces;

namespace Library_Epam.DAL.Repositories
{
    public class CommentRepository:Repository<Comment>, ICommentRepository
    {
        public CommentRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }
    }
}
