﻿using System.Collections.Generic;
using Library_Epam.DAL.Entities.Abstract;

namespace Library_Epam.DAL.Entities
{
    public class Author:BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Article> Articles { get; set; }
    }
}
