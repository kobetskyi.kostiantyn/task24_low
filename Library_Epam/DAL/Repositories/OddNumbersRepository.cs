﻿using Library_Epam.DAL.Entities;
using Library_Epam.DAL.Intefaces;

namespace Library_Epam.DAL.Repositories
{
    public class OddNumbersRepository:Repository<OddNumbers>, IOddNumbersRepository
    {
        public OddNumbersRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }
    }
}
