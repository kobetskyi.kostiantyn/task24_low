﻿using Library_Epam.DAL.Entities.Abstract;

namespace Library_Epam.DAL.Entities
{
    public class OddNumbers:BaseEntity
    {
        public byte One { get; set; }
        public byte Two { get; set; }
        public byte Three { get; set; }

    }
}