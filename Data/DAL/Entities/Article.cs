﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities.Abstract;

namespace Data.Entities
{
    public class Article:BaseEntity
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public DateTime DateTime { get; set; }
        public int AuthorId { get; set; }
        public Author Author{ get; set; }
    }
}
