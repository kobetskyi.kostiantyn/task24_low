﻿namespace Library_Epam.DAL.Entities.Abstract
{
    public abstract class BaseEntity:IBaseEntity
    {
        public int Id { get; set; }
    }
}
