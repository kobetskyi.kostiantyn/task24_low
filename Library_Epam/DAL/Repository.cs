﻿using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using Library_Epam.DAL.Intefaces;

namespace Library_Epam.DAL
{
    public abstract class Repository<T>:IRepository<T> where T :class 
    {
        protected Library_EpamDbContext dbContext { get; set; }

        public Repository(Library_EpamDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public IQueryable<T> GetAll()
        {
            return dbContext.Set<T>();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await dbContext.Set<T>().FindAsync(id);
        }

        public void Add(T entity)
        {
            dbContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
           dbContext.Set<T>().AddOrUpdate(entity);
        }

        public void Delete(T entity)
        {
            dbContext.Set<T>().Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
           Delete(await GetByIdAsync(id));
        }
    }
}
