﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities.Abstract;

namespace Data.Entities
{
    public class Author:BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Article> Articles { get; set; }
    }
}
