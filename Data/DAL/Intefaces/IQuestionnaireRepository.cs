﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Entities;

namespace Data.Intefaces
{
    public interface IQuestionnaireRepository :IRepository<Questionnaire>
    {
        IQueryable<Questionnaire> FindAllWithDetails();
        Task<Questionnaire> FindByIdWithDetails(int id);
    }
}
