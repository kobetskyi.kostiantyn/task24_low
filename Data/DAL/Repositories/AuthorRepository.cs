﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Entities;

namespace Data.Repositories
{
    public class AuthorRepository:Repository<Author>
    {
        public AuthorRepository(Library_EpamDbContext dbContext) : base(dbContext)
        {
        }

    }
}
